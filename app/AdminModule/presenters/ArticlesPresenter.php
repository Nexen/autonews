<?php


namespace App\AdminModule\Presenters;


use App\Presenters\SecuredPresenter;
use Nette\Application\UI\Form;
use Nextras\Forms\Rendering\Bs3FormRenderer;
use Brabijan;

class ArticlesPresenter extends SecuredPresenter
{

	use Brabijan\Images\TImagePipe;


	/** @var  \App\Model\ArticlesRepository @autowire */
	public $articlesRepository;

	/** @var  \App\Model\RelativeArticlesRepository @autowire */
	public $relativeArticlesRepository;

	private $articleId;



	public function renderDefault()
	{
		$this->template->articles = $this->articlesRepository->getAllArticles()->order("postdate DESC");
	}



	public function actionEdit($articleId)
	{
		$this->articleId = $articleId;
		$this->template->articlePhoto = $this->articlesRepository->getArticle($articleId)->photo;
		$this->template->relativeArticles = $this->relativeArticlesRepository->getRelativeArticlesByParent($this->articleId);
	}




	public function createComponentEditArticleForm()
	{
		$form = new Form();


		$data = $this->articlesRepository->getArticle($this->articleId)->toArray();

		$form->addText("title", "Nadpis")
			 ->addRule(Form::FILLED, "Vyplňte titulek!");

		$form->addUpload("photo", "Fotka");

		$form->addCheckbox("offer", "Nabídka");

		$form->addTextArea("text", "Text")
			 ->addRule(Form::FILLED, "Vyplňte text!")
			 ->setAttribute("name", "editor1")
			 ->setAttribute("class", "ckeditor form-control");

		$form->addSubmit("send", "Upravit článek");

		$form->setRenderer(new Bs3FormRenderer());
		$form->setDefaults($data);
		$form->onSuccess[] = $this->editArticleFormSucceeded;

		return $form;
	}



	public function editArticleFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->articlesRepository->editArticle($this->articleId, $values->title, $values->text, $this->user->id, $values->photo, $values->offer);
		$this->flashMessage("Článek byl upraven", "success");
		$this->redirect("Articles:default");
	}



	public function createComponentAddRelativeArticleForm()
	{
		$form = new Form();

		$articles = $this->articlesRepository->getAllArticles()->fetchPairs("id", "title");

		$form->addSelect("selectedArticle", "", $articles)
			 ->setPrompt("Vyberte související produkt")
			 ->setAttribute("class", "selectedArticle selectized");

		$form->addSubmit("send", "Přidat související článek");

		$form->setRenderer(new Bs3FormRenderer());
		$form->onSuccess[] = $this->addRelativeArticleFormSucceeded;

		return $form;
	}



	public function addRelativeArticleFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->relativeArticlesRepository->addRelativeArticle($this->articleId, $values->selectedArticle);
		$this->flashMessage("Související článek byl přidán", "success");
		$this->redirect("Articles:edit", $this->articleId);
	}



	public function createComponentAddArticleForm()
	{
		$form = new Form();

		$form->addText("title", "Nadpis")
			 ->addRule(Form::FILLED, "Vyplňte titulek!");

		$form->addUpload("photo", "Fotka");

		$form->addCheckbox("offer", "Nabídka");

		$form->addTextArea("text", "Text")
			 ->setAttribute("name", "editor1")
			 ->setAttribute("class", "ckeditor form-control");


		$form->addSubmit("send", "Přidat článek");

		$form->setRenderer(new Bs3FormRenderer());
		$form->onSuccess[] = $this->addArticleFormSucceeded;

		return $form;
	}



	public function addArticleFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$row = $this->articlesRepository->addArticle($values->title, $values->text, $this->user->id, $values->photo, $values->offer);
		$this->flashMessage("Článek byl přidán", "success");
		$this->redirect("Articles:edit", $row->id);
	}



	public function handlePublishArticle($id)
	{
		$this->articlesRepository->publishArticle($id);
		$this->flashMessage("Článek byl publikován", "success");
		$this->redirect("Articles:default");
	}



	public function handleUnPublishArticle($id)
	{
		$this->articlesRepository->unPublishArticle($id);
		$this->flashMessage("Článek byl schován", "success");
		$this->redirect("Articles:default");
	}



	public function handleDeleteRelativeArticle($id)
	{
		$this->relativeArticlesRepository->deleteRelativeArticle($id);
		$this->flashMessage("Související Článek byl odepnut", "success");
		$this->redirect("Articles:edit", $this->articleId);
	}



	public function handleDeleteArticle($articleId)
	{
		$this->relativeArticlesRepository->clearRelativeArticle($articleId);
		$this->articlesRepository->deleteArticle($articleId);
		$this->flashMessage("Článek byl smazán.", "success");
		$this->redirect("this");
	}
} 