<?php


namespace App\AdminModule\Presenters;


use App\Presenters\SecuredPresenter;
use Nette\Application\UI\Form;
use Nextras\Forms\Rendering\Bs3FormRenderer;

class ResultsPresenter extends SecuredPresenter
{

	/** @var  \App\Model\SettingsRepository @autowire */
	public $settingsRepository;

	public function createComponentResultsForm()
	{
		$form = new Form();

		$data["results"] = $this->settingsRepository->getSettings("results")->value;

		$form->addTextArea("results", "Dosažené výsledky")
			 ->setAttribute("class", "form-control ckeditor");

		$form->addSubmit("send", "Uložit výsledky");

		$form->setRenderer(new Bs3FormRenderer());
		$form->setDefaults($data);
		$form->onSuccess[] = $this->resultsFormSucceeded;

		return $form;
	}



	public function resultsFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->settingsRepository->update("results", $values->results);
		$this->flashMessage("Výsleky byly upraveny.", "success");
		$this->redirect("this");
	}
} 