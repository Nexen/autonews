<?php


namespace App\AdminModule\Presenters;


use App\Presenters\SecuredPresenter;
use Nette\Application\UI\Form;
use Nextras\Forms\Rendering\Bs3FormRenderer;
use App;
use Brabijan;

class PicturesPresenter extends SecuredPresenter
{


	/** @var  App\Model\PicturesRepository @autowire */
	public $picturesRepository;

	/** @var  App\Model\GalleryRepository @autowire */
	public $galleryRepository;


	use \Brabijan\Images\TImagePipe;

	/** @var \Brabijan\Images\ImageStorage @autowire */
	public $imageStorage;

	public $picture;

	private $categoryId;

	private $category;



	public function injectImageStorage(\Brabijan\Images\ImageStorage $imageStorage)
	{
		$this->imageStorage = $imageStorage;
	}



	public function renderDefault()
	{
		$this->template->categories = $this->galleryRepository->getAllCategories();
	}



	public function renderCategory($categoryId)
	{
		$this->template->category = $this->galleryRepository->getCategory($categoryId);
		$this->template->pictures = $this->picturesRepository->getPicturesByCategory($categoryId);
	}



	public function actionAddPicture($categoryId)
	{
		$this->categoryId = $categoryId;
		$this->template->category = $this->galleryRepository->getCategory($categoryId);
		$this->template->pictures = $this->picturesRepository->getPicturesByCategory($categoryId);
	}



	public function actionEditCategory($categoryId)
	{
		$this->category = $this->galleryRepository->getCategory($categoryId);
		$this->template->category = $this->category;
	}

	public function actionEditPicture($id, $categoryId)
	{
		$this->picture = $this->picturesRepository->getPicture($id);
		$this->template->picture = $this->picture;
		$this->categoryId = $categoryId;
	}



	public function createComponentAddPictureForm()
	{
		$form = new Form();
		$form->addUpload("picture", "Fotografie")
			 ->addRule(Form::IMAGE, 'Avatar musí být JPEG, PNG nebo GIF.')
			 ->addRule(Form::FILLED, "Nahrajte obrázek!");

		$form->addText("description", "Popis");
		$form->addSubmit("send", "Nahrát obrázek");

		$form->setRenderer(new Bs3FormRenderer());
		$form->onSuccess[] = $this->addPictureFormSucceeded;

		return $form;
	}



	public function addPictureFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$imageStore = new \Brabijan\Images\ImageStorage("assets");
		$imageStorage = $imageStore->setNamespace("gallery")->upload($values->picture);

		$this->picturesRepository->addPicture($values->description, basename($imageStorage->getFile()), $this->categoryId);
		$this->flashMessage("Fotka byla přidána.", "success");
		$this->redirect("pictures:category", $this->categoryId);
	}



	public function createComponentEditCategoryForm()
	{
		$form = new Form();
		$form->addUpload("picture", "Fotografie");

		$form->addText("category", "Popis");
		$form->addSubmit("send", "Upravit kategorii");

		$form->setRenderer(new Bs3FormRenderer());
		$form->setDefaults($this->category);
		$form->onSuccess[] = $this->editCategoryFormSucceeded;

		return $form;
	}



	public function editCategoryFormSucceeded(Form $form)
	{
		$values = $form->getValues();
		if ($values->picture->error == 0) {
			$imageStore = new \Brabijan\Images\ImageStorage("assets");
			$imageStorage = $imageStore->setNamespace("gallery")->upload($values->picture);

			$this->galleryRepository->editCategory($this->category->id, $values->category, basename($imageStorage->getFile()));
		} else {
			$this->galleryRepository->editCategory($this->category->id, $values->category);
		}
		$this->flashMessage("Fotka byla přidána.", "success");
		$this->redirect("pictures:category", $this->category->id);
	}

	public function createComponentEditPictureForm()
	{
		$form = new Form();
		$form->addUpload("picture", "Fotografie");

		$form->addText("description", "Popis");
		$form->addSubmit("send", "Upravit obrázek");

		$form->setRenderer(new Bs3FormRenderer());
		$form->setDefaults($this->picture);
		$form->onSuccess[] = $this->editPictureFormSucceeded;

		return $form;
	}



	public function editPictureFormSucceeded(Form $form)
	{
		$values = $form->getValues();
		if ($values->picture->error == 0) {
			$imageStore = new \Brabijan\Images\ImageStorage("assets");
			$imageStorage = $imageStore->setNamespace("gallery")->upload($values->picture);

			$this->picturesRepository->editPicture($this->picture->id, $values->description, basename($imageStorage->getFile()));
		} else {
			$this->picturesRepository->editPicture($this->picture->id, $values->description);
		}
		$this->flashMessage("Fotka byla přidána.", "success");
		$this->redirect("pictures:category", $this->category->id);
	}



	public function createComponentAddCategoryForm()
	{
		$form = new Form();
		$form->addUpload("picture", "Fotografie")
			 ->addRule(Form::IMAGE, 'Avatar musí být JPEG, PNG nebo GIF.')
			 ->addRule(Form::FILLED, "Nahrajte obrázek!");

		$form->addText("category", "Nadpis");
		$form->addSubmit("send", "Přidat kategorii");

		$form->setRenderer(new Bs3FormRenderer());
		$form->onSuccess[] = $this->addCategoryFormSucceeded;

		return $form;
	}



	public function addCategoryFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$imageStore = new \Brabijan\Images\ImageStorage("assets");

		$imageStorage = $imageStore->setNamespace("gallery")->upload($values->picture);

		$row = $this->galleryRepository->addCategory($values->category, basename($imageStorage->getFile()));
		$this->picturesRepository->addPicture($values->category, basename($imageStorage->getFile()), $row->id);
		$this->flashMessage("Kategorie byla přidána.", "success");
		$this->redirect("pictures:category", $row->id);
	}



	public function handleDeleteCategory($id)
	{
		$categories = $this->picturesRepository->getPicturesByCategory($id);

		foreach ($categories as $category) {
			$this->imageStorage->deleteFile($category->picture);
			$this->picturesRepository->deletePictureByCategory($id);
		}
		$this->galleryRepository->deleteCategory($id);
		$this->flashMessage("Kategorie a fotky byly smazány.", "success");
		$this->redirect("this");
	}



	public function handleDeletePicture($id)
	{
		$picture = $this->picturesRepository->getPicture($id);

		$this->imageStorage->deleteFile($picture->picture);
		$this->picturesRepository->deletePicture($id);
		$this->flashMessage("Fotka byla smazána.", "success");
		$this->redirect("this");
	}
} 