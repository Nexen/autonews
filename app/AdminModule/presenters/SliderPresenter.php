<?php


namespace App\AdminModule\Presenters;


use App\Presenters\SecuredPresenter;
use Nette\Application\UI\Form;
use Nextras\Forms\Rendering\Bs3FormRenderer;

class SliderPresenter extends SecuredPresenter
{

	/** @var  \App\Model\SliderRepository @autowire */
	public $sliderRepository;

	use \Brabijan\Images\TImagePipe;

	/** @var \Brabijan\Images\ImageStorage @autowire */
	public $imageStorage;



	public function injectImageStorage(\Brabijan\Images\ImageStorage $imageStorage)
	{
		$this->imageStorage = $imageStorage;
	}



	public function renderDefault()
	{
		$this->template->slides = $this->sliderRepository->getAllSlides()->order("order ASC");
	}



	public function createComponentAddSlideForm()
	{
		$form = new Form();
		$form->addUpload("picture", "Fotografie")
			 ->addRule(Form::IMAGE, 'Slide musí být JPEG, PNG nebo GIF.')
			 ->addRule(Form::FILLED, "Nahrajte obrázek!");

		$form->addText("text", "Text");

		$form->addSubmit("send", "Nahrát obrázek");

		$form->setRenderer(new Bs3FormRenderer());
		$form->onSuccess[] = $this->addSlideFormSucceeded;

		return $form;
	}



	public function addSlideFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$row = $this->sliderRepository->addSlide($values->picture, $values->text);
		$this->sliderRepository->setOrderToSlide($row->id);
		$this->flashMessage("Fotka byla přidána.", "success");
		$this->redirect("slider:default");
	}



	public function handleDeleteSlide($id)
	{
		$slide = $this->sliderRepository->getSlide($id);

		$this->imageStorage->deleteFile($slide->picture);
		$this->sliderRepository->deleteSlide($id);
		$this->flashMessage("Slide byl smazán.", "success");
		$this->redirect("this");
	}



	public function handleMoveUp($id)
	{
		$this->sliderRepository->moveUp($id);
		$this->flashMessage("Slide byl posunut nahoru", "success");
		$this->redirect("this");
	}



	public function handleMoveDown($id)
	{
		$this->sliderRepository->moveDown($id);
		$this->flashMessage("Slide byl posunut dolu", "success");
		$this->redirect("this");
	}
} 