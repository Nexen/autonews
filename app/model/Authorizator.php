<?php


namespace App\Model;


class Authorizator extends \Nette\Security\Permission{

	public function __construct()
	{
		$this->addRole("guest");
		$this->addRole("admin");

		$this->addResource("Admin:Articles");
		$this->addResource("Admin:Homepage");
		$this->addResource("Admin:Pictures");
		$this->addResource("Admin:Results");
		$this->addResource("Admin:Slider");

		$this->deny(self::ALL, self::ALL, self::ALL);

		$this->allow("admin", self::ALL, self::ALL);
	}
} 