<?php


namespace App\Model;


use App\BaseRepository;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

class ArticlesRepository extends BaseRepository
{

	public function getArticlesCount()
	{
		return $this->findAll()->count();
	}



	public function getAllArticles()
	{
		return $this->findAll();
	}



	public function getAllPublishedArticles()
	{
		return $this->findAll()->where("published = 1");
	}



	public function getArticle($articleId)
	{
		return $this->find($articleId);
	}



	public function deleteArticle($articleId)
	{
		return $this->find($articleId)->delete();
	}



	public function editArticle($articleId, $title, $text, $author, $photo, $offer)
	{
		if ($photo->error == 4) {
			$data = array(
				"title" => $title,
				"text" => $text,
				"author" => $author,
				"offer" => $offer,
				"webalized" => Strings::webalize($title),
				"postdate" => new DateTime()
			);
		} else {
			$imageStore = new \Brabijan\Images\ImageStorage("assets");
			$imageStorage = $imageStore->setNamespace("articleImages")->upload($photo);

			$data = array(
				"photo" => basename($imageStorage->getFile()),
				"title" => $title,
				"text" => $text,
				"author" => $author,
				"offer" => $offer,
				"webalized" => Strings::webalize($title),
				"postdate" => new DateTime()
			);
		}

		return $this->find($articleId)->update($data);
	}



	public function addArticle($title, $text, $author, $photo, $offer)
	{
		if ($photo->error == 4) {
			$data = array(
				"title" => $title,
				"text" => $text,
				"author" => $author,
				"offer" => $offer,
				"webalized" => Strings::webalize($title),
				"postdate" => new DateTime()
			);
		} else {
			$imageStore = new \Brabijan\Images\ImageStorage("assets");
			$imageStorage = $imageStore->setNamespace("articleImages")->upload($photo);

			$data = array(
				"photo" => basename($imageStorage->getFile()),
				"title" => $title,
				"text" => $text,
				"author" => $author,
				"offer" => $offer,
				"webalized" => Strings::webalize($title),
				"postdate" => new DateTime()
			);
		}

		return $this->getTable()->insert($data);
	}



	public function publishArticle($id)
	{
		$data = array(
			"publishDate" => new DateTime(),
			"published" => 1
		);

		return $this->find($id)->update($data);
	}



	public function unPublishArticle($id)
	{
		$data = array(
			"publishDate" => new DateTime(),
			"published" => 0
		);

		return $this->find($id)->update($data);
	}



	public function getArticlesCountByCategory($categoryId)
	{
		return $this->findBy(array(
			"categoryId" => $categoryId
		))          ->count();
	}



	public function getAllArticlesByCategory($categoryId)
	{
		return $this->findBy(array(
			"categoryId" => $categoryId
		));
	}
} 