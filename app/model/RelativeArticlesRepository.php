<?php


namespace App\Model;


use App\BaseRepository;

class RelativeArticlesRepository extends BaseRepository
{

	public function addRelativeArticle($parentArticle, $relativeArticle)
	{
		$data = array(
			"parentArticle" => $parentArticle,
			"relativeArticle" => $relativeArticle
		);

		return $this->getTable()->insert($data);
	}



	public function getRelativeArticlesByParent($parentArticleId)
	{
		$data = array(
			"parentArticle" => $parentArticleId
		);

		return $this->findBy($data);
	}



	public function deleteRelativeArticle($id)
	{
		return $this->find($id)->delete();
	}



	public function clearRelativeArticle($parentId)
	{
		return $this->getTable()->where("parentArticle = ? OR relativeArticle = ?", $parentId, $parentId)->delete();
	}
} 