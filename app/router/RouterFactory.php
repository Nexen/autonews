<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList();
		$router[] = $adminRouter = new RouteList("Admin");
		$adminRouter[] = new Route("admin/<presenter>[/<action>][/<id>]", 'Homepage:default');
		$router[] = new Route('/clanek[/<action>][/<id>-<webalized>]', array(
			'presenter' => "Article",
			'action' => array(
				Route::VALUE => 'default'
			)
		));

		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');

		return $router;
	}

}
