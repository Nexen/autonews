<?php


namespace App\Presenters;

use App\Model;
use Brabijan;

class ArticlePresenter extends BasePresenter
{

	use Brabijan\Images\TImagePipe;

	/** @var  Model\ArticlesRepository @autowire */
	public $articlesRepository;

	/** @var  Model\RelativeArticlesRepository @autowire */
	public $relativeArticlesRepository;



	public function renderView($id, $webalized)
	{
		$this->template->article = $this->articlesRepository->getArticle($id);
		$this->template->relativeArticles = $this->relativeArticlesRepository->getRelativeArticlesByParent($id);
	}
} 