<?php

namespace App\Presenters;

use Nette,
	App\Model;
use Brabijan;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	use Brabijan\Images\TImagePipe;
	/** @var  Model\ArticlesRepository @autowire */
	public $articlesRepository;

	public function renderDefault()
	{
		$this->template->articles = $this->articlesRepository->getAllPublishedArticles()->order("publishDate DESC");
	}

}
